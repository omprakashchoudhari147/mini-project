Feature: Check user is able to create account

  @login
  Scenario Outline: Create account with API and validate it with on UI
    When I send post request body with "<ValidUsername>" and "<ValidPassword>" to Login API
    Then I get status code as 201
    And I get "<ValidUsername>" in response body
    Given I am on Login Page
    When I enter "<ValidUsername>" and "<ValidPassword>" on login Page
    And I click on the Login button
    Then I should see my "<ValidUsername>" on the Books Dashboard page

    Examples: 
      | ValidUsername | ValidPassword |
      | Omprak1231515 | Om@jio123!A   |

  @login
  Scenario Outline: Create account with invalid credentials
    When I send post request body with "<ValidUsername>" and "<inValidPassword>" to Login API
    Then I get status code as 400
    And I get "Passwords must have at least one non alphanumeric character, one digit ('0'-'9'), one uppercase ('A'-'Z'), one lowercase ('a'-'z'), one special character and Password must be eight characters or longer." message in response body

    Examples: 
      | ValidUsername | ValidPassword |
      | Omprak1231512 | String        |
