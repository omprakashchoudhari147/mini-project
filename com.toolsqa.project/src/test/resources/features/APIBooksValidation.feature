Feature: Make an API call BookStore API and capture the details of all the books

  @books
  Scenario: Resquest and store BookStore response body
    When I send get request to BookStore API
    Then I get status code as 200
    Given I am on Book Web UI Page
    When I search and click on the book then details should match with api response
