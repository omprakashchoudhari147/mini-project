package webdriverfactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxDriverFactory implements WebDriverFactory{

	@Override
	public WebDriver getDriver() {
		return new FirefoxDriver();
	}

}
