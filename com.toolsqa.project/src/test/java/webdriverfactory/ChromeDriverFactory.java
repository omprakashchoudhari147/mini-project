package webdriverfactory;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import constants.Constants;

public class ChromeDriverFactory implements WebDriverFactory{

	@Override
	public WebDriver getDriver() {
		System.setProperty("webdriver.chrome.driver", Constants.CHROMEDRIVERPATH);
		ChromeOptions options = new ChromeOptions();
		options.addExtensions(new File("./src/test/resources/ChromeExtension/adblocker.crx"));
		options.addArguments("--remote-allow-origins=*");
		return new ChromeDriver(options);
	}

}
