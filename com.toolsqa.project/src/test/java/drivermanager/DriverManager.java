package drivermanager;

import org.openqa.selenium.WebDriver;

import exceptions.BrowserNotFound;
import utilities.ConfigReader;
import webdriverfactory.ChromeDriverFactory;
import webdriverfactory.FirefoxDriverFactory;

public class DriverManager {

	private DriverManager() {
		super();
	}

	private static WebDriver driver;

	public static WebDriver getDriver() {
		if (driver == null) {
			synchronized (DriverManager.class) {
				if (driver == null) {
					String browserName = ConfigReader.getBrowser();
					switch (browserName.toUpperCase()) {
					case "CHROME":
						driver = new ChromeDriverFactory().getDriver();
						break;
					case "FIREFOX":
						driver = new FirefoxDriverFactory().getDriver();
						break;
					default:
						throw new BrowserNotFound("Unsupported Browser : " + browserName.toUpperCase());
					}
				}
			}
		}
		return driver;
	}

	public static void setDriver(WebDriver driver) {
		DriverManager.driver = driver;
	}

}
