package stepdefinations;

import constants.APIConstants;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import utilities.ToolsQAPicoContainer;

public class BookAPIStoreStep {
	private ToolsQAPicoContainer toolsQAPicoContainer;

	public BookAPIStoreStep(ToolsQAPicoContainer toolsQAPicoContainer) {
		this.toolsQAPicoContainer = toolsQAPicoContainer;
	}

	@When("I send get request to BookStore API")
	public void i_send_get_request_to_book_store_api() {
		Response response = RestAssured.given().when().get(APIConstants.BooksAPI);
		VerifyAPIRespone.responseMap.put(toolsQAPicoContainer.getScenario().getName(), response);
	}

}
