package stepdefinations;

import java.util.HashMap;

import constants.APIConstants;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import utilities.ToolsQAPicoContainer;

public class LoginAPIStep {
	private ToolsQAPicoContainer toolsQAPicoContainer;

	public LoginAPIStep(ToolsQAPicoContainer toolsQAPicoContainer) {
		this.toolsQAPicoContainer = toolsQAPicoContainer;
	}

	@When("I send post request body with {string} and {string} to Login API")
	public void i_send_post_request_body_with_and_to_login_api(String username, String password) {
		HashMap<String, String> user = new HashMap<String, String>();
		user.put("userName", username);
		user.put("password", password);

		Response response = RestAssured.given().contentType("application/json").body(user).when()
				.post(APIConstants.LoginAPI);

		VerifyAPIRespone.responseMap.put(toolsQAPicoContainer.getScenario().getName(), response);
	}

}
