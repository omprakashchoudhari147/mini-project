package stepdefinations;

import java.util.HashMap;

import org.testng.Assert;

import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import utilities.ToolsQAPicoContainer;

public class VerifyAPIRespone {
	public static HashMap<String, Response> responseMap = new HashMap<String, Response>();
	private ToolsQAPicoContainer toolsQAPicoContainer;

	public VerifyAPIRespone(ToolsQAPicoContainer toolsQAPicoContainer) {
		this.toolsQAPicoContainer = toolsQAPicoContainer;
	}

	@Then("I get status code as {int}")
	public void i_get_status_code_as(Integer statusCode) {
		Response response = VerifyAPIRespone.responseMap.get(toolsQAPicoContainer.getScenario().getName());
		response.then().statusCode(statusCode);

	}

	@Then("I get {string} in response body")
	public void i_get_in_response_body(String expectedUsername) {
		Response response = VerifyAPIRespone.responseMap.get(toolsQAPicoContainer.getScenario().getName());
		String actualUsername = response.jsonPath().getString("username");
		Assert.assertEquals(actualUsername, expectedUsername);
	}
	
	@Then("I get {string} message in response body")
	public void i_get_message_in_response_body(String expectedMessage) {
		Response response = VerifyAPIRespone.responseMap.get(toolsQAPicoContainer.getScenario().getName());
		String actualmessage = response.jsonPath().getString("message");
		Assert.assertEquals(actualmessage, expectedMessage);
	}
}
