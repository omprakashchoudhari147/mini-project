package stepdefinations;

import org.testng.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utilities.ToolsQAPicoContainer;

public class LoginWebUIStep {
	private ToolsQAPicoContainer toolsQAPicoContainer;

	public LoginWebUIStep(ToolsQAPicoContainer toolsQAPicoContainer) {
		this.toolsQAPicoContainer = toolsQAPicoContainer;
	}

	@Given("I am on Login Page")
	public void i_am_on_login_page() {
		toolsQAPicoContainer.getLoginPage().open();
	}

	@When("I enter {string} and {string} on login Page")
	public void i_enter_and_on_login_page(String username, String password) {
		toolsQAPicoContainer.getLoginPage().enterUserName(username);
		toolsQAPicoContainer.getLoginPage().enterPassword(password);
	}

	@When("I click on the Login button")
	public void i_click_on_the_login_button() throws InterruptedException {
		toolsQAPicoContainer.getLoginPage().clickOnLoginButton();
	}

	@Then("I should see my {string} on the Books Dashboard page")
	public void i_should_see_my_on_the_books_dashboard_page(String expectedUserName) {
		Assert.assertEquals(toolsQAPicoContainer.getProfilePage().getUserNameValue(), expectedUserName);

	}
}
