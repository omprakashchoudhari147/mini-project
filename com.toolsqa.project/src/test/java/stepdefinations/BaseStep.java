package stepdefinations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import drivermanager.DriverManager;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import utilities.ToolsQAPicoContainer;

public class BaseStep {

	private static final Logger LOGGER = LogManager.getLogger(BaseStep.class);
	private ToolsQAPicoContainer toolsQAPicoContainer;

	public BaseStep(ToolsQAPicoContainer toolsQAPicoContainer) {
		this.toolsQAPicoContainer = toolsQAPicoContainer;
	}

	@Before
	public void stepUp(Scenario scenario) {
		LOGGER.info("Initialize Page Object Model using PicoContainer");
		toolsQAPicoContainer.setDriver(DriverManager.getDriver());
		toolsQAPicoContainer.initializePageObject(scenario);
		toolsQAPicoContainer.getDriver().manage().window().maximize();
	}

	@AfterStep
	public void takeScreenshots(Scenario scenario) {
		if (scenario.isFailed()) {
			LOGGER.info("Scenario failed take the screenshot");
			byte[] screenshot = ((TakesScreenshot) toolsQAPicoContainer.getDriver()).getScreenshotAs(OutputType.BYTES);
			scenario.attach(screenshot, "image/png", scenario.getName());
		}
	}

	@After
	public void tearUp() {
		LOGGER.info("Closing the WebDriver");
		toolsQAPicoContainer.getDriver().quit();
		DriverManager.setDriver(null);
	}

}
