package stepdefinations;

import java.util.List;

import org.testng.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import pages.BooksPage;
import responsepojo.BookPojo;
import utilities.ToolsQAPicoContainer;

public class BookUIStoreStep {
	private ToolsQAPicoContainer toolsQAPicoContainer;
	private BooksPage booksPage;

	public BookUIStoreStep(ToolsQAPicoContainer toolsQAPicoContainer) {
		this.toolsQAPicoContainer = toolsQAPicoContainer;
		this.booksPage = toolsQAPicoContainer.getBooksPage();
	}

	@Given("I am on Book Web UI Page")
	public void i_am_on_book_web_ui_page() {
		booksPage.open();
	}

	@When("I search and click on the book then details should match with api response")
	public void i_search_and_click_on_the_book_then_details_should_match_with_api_response() {
		Response response = VerifyAPIRespone.responseMap.get(toolsQAPicoContainer.getScenario().getName());
		List<BookPojo> books = response.jsonPath().getList("books", BookPojo.class);
		validateJsonResponse(books);
	}
	
	public void validateJsonResponse(List<BookPojo> books) {
		for (BookPojo book : books) {
			booksPage.searchBar(book.getTitle());
			Assert.assertEquals(book.getTitle(), booksPage.getBookTitle());
			Assert.assertEquals(book.getAuthor(), booksPage.getBookAuthor());
			Assert.assertEquals(book.getPublisher(), booksPage.getBookPublisher());
			toolsQAPicoContainer.getDriver().navigate().refresh();
		}
	}

}
