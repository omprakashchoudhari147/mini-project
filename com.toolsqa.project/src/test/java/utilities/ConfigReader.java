package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import constants.Constants;


public class ConfigReader {
	private static Properties configProperties;

	static {
		String browserPropertiesFilePath = Constants.CONFIGPATH;
		try {
			FileInputStream browserPropertiesInputStream = new FileInputStream(browserPropertiesFilePath);
			configProperties = new Properties();
			configProperties.load(browserPropertiesInputStream);
			browserPropertiesInputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String getBrowser() {
		return configProperties.getProperty("browser");
	}
}
