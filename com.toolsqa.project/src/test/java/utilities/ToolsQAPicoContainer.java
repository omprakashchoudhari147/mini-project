package utilities;

import org.openqa.selenium.WebDriver;

import io.cucumber.java.Scenario;
import pages.BooksPage;
import pages.LoginPage;
import pages.ProfilePage;

public class ToolsQAPicoContainer {
	private WebDriver driver;
	private BooksPage booksPage;
	private ProfilePage profilePage;
	private LoginPage loginPage;
	private Scenario scenario;
	
	public Scenario getScenario() {
		return scenario;
	}
	
	public ProfilePage getProfilePage() {
		return profilePage;
	}

	public LoginPage getLoginPage() {
		return loginPage;
	}

	public BooksPage getBooksPage() {
		return booksPage;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	public void initializePageObject(Scenario scenario) {
		booksPage = new BooksPage(driver);
		loginPage = new LoginPage(driver);
		profilePage = new ProfilePage(driver);
		this.scenario = scenario;
	}
}
