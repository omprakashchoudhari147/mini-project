package baserunner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(plugin = { "io.qameta.allure.cucumber7jvm.AllureCucumber7Jvm", "pretty" }, glue = {
		"stepdefinations" }, features = { "./src/test/resources/features/" }, tags = "@login")
public class BaseRunnerLogin extends AbstractTestNGCucumberTests {

}
