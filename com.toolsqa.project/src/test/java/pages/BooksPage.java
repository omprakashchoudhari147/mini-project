package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import constants.UIConstants;

public class BooksPage extends AbstractPage {

	public BooksPage(WebDriver driver) {
		super(driver);
	}

	@FindBy(id = "searchBox")
	private WebElement searchBar;

	@FindBy(xpath = "//*[@class='mr-2']/a[@href]")
	private WebElement bookButton;

	@FindBy(xpath = "//*[@class='rt-tr -odd']//*[@class='rt-td']")
	private List<WebElement> bookInfo;

	public void searchBar(String book) {
		searchBar.sendKeys(book);
	}

	public void clickOnBook() {
		bookButton.click();
	}

	public String getBookTitle() {
		return bookInfo.get(1).getText();
	}

	public String getBookAuthor() {
		return bookInfo.get(2).getText();
	}

	public String getBookPublisher() {
		return bookInfo.get(3).getText();
	}

	public void open() {
		driver.get(UIConstants.BOOKSURL);
	}
}
