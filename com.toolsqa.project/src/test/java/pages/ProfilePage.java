package pages;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProfilePage extends AbstractPage {

	public ProfilePage(WebDriver driver) {
		super(driver);
	}

	@FindBy(id = "userName-value")
	private WebElement userNameValue;

	public String getUserNameValue() {
		return new WebDriverWait(driver, Duration.ofSeconds(10))
				.until(ExpectedConditions.elementToBeClickable(userNameValue)).getText();
	}

}
