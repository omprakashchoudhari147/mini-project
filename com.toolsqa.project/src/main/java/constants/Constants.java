package constants;

public class Constants {
public static final String CHROMEDRIVERPATH = "./src/test/resources/Drivers/chromedriver.exe";
public static final String CONFIGPATH = "./src/test/resources/Browser.properties";
}
