package constants;

public class APIConstants {
	public static final String LoginAPI = "https://demoqa.com/Account/v1/User";
	public static final String BooksAPI = "https://demoqa.com/BookStore/v1/Books";
}
