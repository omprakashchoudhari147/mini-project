package exceptions;

public class BrowserNotFound extends RuntimeException{

	public BrowserNotFound(String message) {
		super(message);
	}

}
